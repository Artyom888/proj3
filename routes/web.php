<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'NewsController@getAll');
Route::get('post/{id}','NewsController@show')->name('showPosts');
Route::post('/post/comments','NewsController@createComment');
Route::post('/post/likes','NewsController@updateLikes');


Route::group(['prefix' => 'user'], function() {
    Route::get('/register',function (){return view('UsersAuthView/register');})->name('userRegisterView');
    Route::post('/register','UserAuth\RegisterController@register')->name('userRegister');

    Route::get('/login',function (){return view('UsersAuthView/login');})->name('userLoginView');
    Route::post('/login','UserAuth\loginController@login')->name('userLogin');
    Route::post('/logout','UserAuth\loginController@logout')->name('userLogout');
});

Route::get('/login',function (){return view('AdminAuthView/login');})->name('adminLoginView');
Route::post('/login','AdminAuth\LoginController@login')->name('adminLogin');
Route::post('/logout','AdminAuth\LoginController@logout')->name('adminLogout');
//ADMIN
Route::group (['prefix'=>'admin','middleware'=>['adminAuth']],function(){
    Route::get('/',['middleware'=>'checkRole','roles'=> ['admin','superAdmin','postEditor'],function (){return view('Admin/home');}])->name('homePage');
//Posts dtb
    Route::group(['prefix' => 'posts','middleware'=>['checkRole'],'roles'=> ['admin','superAdmin','postEditor']], function() {
        Route::get('/', function () {return view('Admin/show');})->name('allPosts');
        Route::get('/dtb',        'Admin\PostsController@getAll');
        Route::get('/dtb/select', 'Admin\PostsController@select');
        Route::post('/edit',      'Admin\PostsController@view');
        Route::post('/edit/add',  'Admin\PostsController@updatePost');
        Route::post('delete',     'Admin\PostsController@destroy');
        Route::get('/export',     'Admin\PostsController@export')->name('export');
    });
//Post create
    Route::group(['prefix' => 'post','middleware'=> ['checkRole'],'roles'=> ['admin','superAdmin']], function() {
        Route::get('create','Admin\PostsController@getCatForCreate')->name('newsCreate');
        Route::post('create','Admin\PostsController@addPost')->name('addNews');
    });
//Post Category
    Route::group(['prefix' => 'category','middleware'=>['checkRole'],'roles'=> ['admin','superAdmin']], function() {
        Route::get('/',function (){
            return view('Admin/category');
        })->name('category');
        Route::get('dtb','Admin\CategoryController@getAll');
        Route::post('delete','Admin\CategoryController@destroy');
        Route::get('create','Admin\CategoryController@create');
        Route::post('create','Admin\CategoryController@add')->name('add-category');
        Route::post('edit','Admin\CategoryController@viewEdit');
        Route::post('edit/add','Admin\CategoryController@update')->name('add-changes');
        Route::get('export','Admin\CategoryController@export')->name('export_categories');

    });
});
