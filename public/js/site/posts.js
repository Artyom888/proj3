$(function () {
//User Comments
    $('form').on('click', '#addComment', function (e) {
        e.preventDefault();
        var postId = $(this).val();
        var commentText =  $(this).prev().val();
        var data =  {postId:postId,text:commentText};
        $.ajax({
            type: "POST",
            url: "/post/comments" ,
            data: data,
            success: function (data) {
                if(data.success) {
                    location.reload();
                }else {
                 $('#commentsErr').attr('class', 'alert alert-danger').html('No comment to add');
                }
            }
        });
    });
//post  likes
    $('body').on('click', '.likeBtn', function () {
        var likeBtn = $(this);
        var idStr = $(this).attr('id');
        var postId = idStr.split('_');
        postId = postId[1];
        var badge = $(this).next();
        var data = {id: postId};
        $.ajax({
            type: "POST",
            url: "/post/likes",
            data: data,
            success: function (data) {
                if (data.success) {
                    badge.html(data.quantity.likes);
                    likeBtn.attr('disabled', true).css("cursor", "default");
                }
            }
        });
    });
    if(window.performance && window.performance.navigation.type === 2)
    {
        window.location.reload();
    }
});