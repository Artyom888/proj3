var catTable;
$(function () {
    catTable = $("#catDtb").DataTable({
        searchPane: true,
        ajax: "/admin/category/dtb",
        processing: true,
        serverSide: true,
        columns:[
            {"data": "created_at"},
            {"data": "name"},
            {"data": null,
                render: function (data, type, row) {
                    return '<span><button class="editBtn" type="submit" value="' + data.id + '">Edit</button></span>';
                }
            },
            {"data": null,
                render: function (data, type, row) {
                    return '<span><button class="removeBtn" type="submit" value="' + data.id + '">Delete</button></span>';
                }
            }
        ]
    });

// News : Create Category
    $('#catCreateBtn').click(function () {
        $.ajax({
            type: "GET",
            url: "/admin/category/create",
            success:function (data) {
                $('#createCatModal .modal-body').html(data);
            }
        });
    });

// Add new category
    $('body').on('click', '.addCatBtn', function () {
        var createCatName = $('#createName').val();
        var createCatDesc = $('#createDesc').val();
        var data = {name:createCatName,desc:createCatDesc};
        $.ajax({
            type: "POST",
            url: "/admin/category/create",
            data:data,
            success:function (data) {
                if(data.success){
                    catTable.ajax.reload();
                    $('#createResult').attr('class','text-success text-center lead').text( data.message);
                    setTimeout(function(){
                        $('.catCloseBtn').click();
                    }, 2000);
                }else{
                    $('#createResult').attr('class','text-danger text-center lead').text( data.message);
                }
            }
        });
    });

    //News Category Datatable Edit Category
    var editRowId;
    $('#catDtb').on('click', '.editBtn', function () {
        $('#updateCat').text('');
        var row = $(this).parents('tr')[0];
        editRowId = $(this).val();
        $.ajax({
            type: "POST",
            url: "/admin/category/edit",
            data:{id:editRowId},
            success:function (data) {
                $('#editCatModal .modal-body').html(data);
            }
        });
        $("#editCatModal").modal('show');

    });
//Update Category
    $('body').on('click', '#addEditedCat', function () {
        var editCatName = $('#editCatName').val();
        var editDesc = $('#editDesc').val();
        var data = {name:editCatName,desc:editDesc,id:editRowId};
        $.ajax({
            type: "POST",
            url: "/admin/category/edit/add",
            data:data,
            success:function (data) {
                // $('#updateCat').text(data.message);
                if(data.success){
                    catTable.ajax.reload();
                    $('#updateCat').attr('class','text-success text-center lead').text( data.message);
                    setTimeout(function(){
                        $('.editCloseBtn').click();
                    }, 2000);
                }else{
                    $('#updateCat').attr('class','text-danger text-center lead').text( data.message);
                }

            }
        });
    });
//News Category Datatable row remove
    $('#catDtb').on('click', '.removeBtn', function () {
        var row = $(this).parents('tr')[0];
        var rowId = $(this).val();
        var data = {id:rowId};
        if (rowId && confirm('Admin: Delete this category ?')) {
            $.ajax({
                type: "POST",
                url: "/admin/category/delete",
                data:data,
                success: function (data) {
                    if (data.success) {
                        row.remove();
                        catTable.ajax.reload();
                    }
                }
            });
        }
    });

});