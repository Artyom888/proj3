var table;
$(function () {
    table = $("#newsDtb").DataTable({
        searchPane: true,
        ajax: "/admin/posts/dtb",
        processing: true,
        serverSide: true,
        ordering: true,
        fnServerParams: function (data) {
            var ctgId = $("#catFilter option:selected").val();
            if (ctgId) {
                data.ctg = ctgId;
            }
        },
        columns:[
            {"data": "created_at"},
            {"data": "title"},
            {"data": "desc"},
            {"data": "news_content"},
            {"data": "likes"},
            {"data": null,
                render: function (data, type, row) {
                    if (data.image) {
                        return '<img src="/thumbs/' + data.image + '" class="dtb-img" >';
                    } else {
                        return '<img src="/thumbs/unknown_picture.jpg" class="dtb-img">';
                    }
                }
            },

            {"data": null,
                render: function (data, type, row) {
                    return '<span><button class="newsEditedBtn" type="submit" value="'+ data.id + '">Edit</button></span>';
                }
            },
            {"data": null,
                render: function (data, type, row) {
                    return '<span><button class="removeBtn" type="submit" value="'+ data.id + '">Delete</button></span>';
                }
            }
        ]
    });

    $.ajax({
        type: "GET",
        url: '/admin/posts/dtb/select',
        success: function (data) {
            var result = JSON.parse(data);
            var i;
            for (i = 0; i < result.length; i++) {
                $('#catFilter').append("<option value='" + result[i].id + "'>" + result[i].name + "</option>");
            }
        }
    });

// // News Datatable filtering
    $('#catFilter').change(function () {
        var catId2 = $("#catFilter option:selected").val();
        if(catId2){
            table.ajax.reload();
        }
    });


//News Datatable row remove
    $('#newsDtb').on('click', '.removeBtn', function () {
        var row = $(this).parent('tr');
        var rowId = $(this).val();
        var data = {id: rowId};
        if (rowId && confirm('Admin: Delete this news ?')) {
            $.ajax({
                type: "POST",
                url: "/admin/posts/delete",
                data: data,
                success: function (data) {
                    if (data.success) {
                        row.remove();
                        table.ajax.reload();
                    }
                }
            });
        }
    });

// News datatable eidt posts
var editRowId;
$('#newsDtb').on('click', '.newsEditedBtn', function () {
    $('#updatePost').text('');
    editRowId = $(this).val();
    $.ajax({
        type: "POST",
        url: "/admin/posts/edit",
        data: {id: editRowId},
        success: function (data) {
            $('#editPostModal .modal-body').html(data);
        }
    });
    $("#editPostModal").modal('show');
});

//Update post
    $('body').on('click', '#addEditedPost', function () {
        var title = $('#editPostTitle').val();
        var desc = $('#editPostDesc').val();
        var content = $('#editPostContent').val();
        var likes = $('#editLikes').val();
        var data = {title: title, desc: desc, content: content,likes:likes, id: editRowId};
        $.ajax({
            type: "POST",
            url: "/admin/posts/edit/add",
            data: data,
            success: function (data) {
                if(data.success){
                    table.ajax.reload();
                    $('#updatePost').attr('class','text-success text-center lead').text( data.message);
                    setTimeout(function(){
                    $('.closeBtn').click();
                    }, 2000);
                }else{
                    $('#updatePost').attr('class','text-danger text-center lead').text( data.message);
                }

            }
        });
    });
// Textarea Editor
    ClassicEditor
        .create( document.querySelector( '#areaEditor' ) )
        .catch((err) => {
        console.log(err)
    });

});
