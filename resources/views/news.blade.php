@extends('Layouts.posts')
@section('content')

    <div class="container cont-content ">
        <h2 class="text-center text-capitalize"> View all news </h2>
        <div class="row rows">
            @if(isset(Auth::guard('user')->user()->likedPosts))
                @foreach (Auth::guard('user')->user()->likedPosts as $item)
                    <? $likeIds[] = $item->id ?>
                @endforeach
            @endif
            @foreach(($posts) as $post)
                <div class="col-xs-6 col-sm-3 col-md-2 col-lg-2" >
                    <div>
                        @if($post->image)
                            <a href="{{route('showPosts',['id'=>$post->id]) }}"><img src="photos/{{ $post->image }}" class="post-img center-block" alt="post image"></a>
                        @else
                            <a href="{{route('showPosts',['id'=>$post->id]) }}"><img src="photos/unknown_picture.jpg" class="post-img center-block"  alt="post image"></a>
                        @endif
                    </div>
                    <hr class="">
                    <div class="post-field">
                        <h3 class="news-title">{{ str_limit(($post->title),15) }}</h3>
                        <p class="news-desc">{{str_limit(($post->desc),17)}}</p>
                    </div>
                    <div class="read-more">
                        <a href="{{route('showPosts',['id'=>$post->id]) }}" >
                            <button class="btn btn-info btn-sm">Read more</button>
                        </a>
                    </div>
                    <div>
                        @if(Auth::guard('user')->check())
                            @if(isset($likeIds))
                                @if(in_array( $post->id, $likeIds))
                                    <button type="button"  id="post_{{ $post->id }}" disabled class="btn btn-primary btn-xs glyphicon glyphicon-thumbs-up  likeBtn" > Like</button>
                                @else
                                    <button type="button"  id="post_{{ $post->id }}" class="btn btn-primary btn-xs glyphicon glyphicon-thumbs-up likeBtn" > Like</button>
                                @endif

                            @else
                                <button type="button"  id="post_{{ $post->id }}" class="btn btn-primary btn-xs glyphicon glyphicon-thumbs-up likeBtn" > Like</button>
                            @endif
                        @endif
                        <p class="badge likeCount">{{$post->likes}}</p>
                    </div>

                </div>
            @endforeach

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul class="pagination pagination-lg">
                {{$posts}}
            </ul>
        </div>
    </div>


@endsection
