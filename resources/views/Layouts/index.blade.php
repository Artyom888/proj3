<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>News</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{asset('bootstrap-3/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('bootstrap-3/css/bootstrap-theme.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('DataTables/DataTables/css/jquery.dataTables.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/news.css')}}">

</head>
<body>

<div id="app">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/admin') }}"> News</a>

            </div>
            @if(Auth::guard('admin')->check())
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                            {{ Auth::guard('admin')->user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('adminLogout') }}"
                                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('adminLogout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>
            @endif
        </div>
    </nav>
</div>

<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 left-sdb">
    <nav class="navbar navbar-default sidebar" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

            </div>
        <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li ><a href="/">Visit Site<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-align-justify"></span></a></li>
                    <li class="active"><a href="/admin">Home Page<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-home"></span></a></li>
                    <li class="dropdown">
                        <a href="#"  class="dropdown-toggle" data-toggle="dropdown"> Posts <span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span></a>
                            <ul class="dropdown-menu forAnimate" role="menu">
                                <li class="divider"></li>
                                <li><a href="{{route('allPosts')}}">Posts Table</a></li>
                                <li class="divider"></li>
                                <li><a href="{{ route('newsCreate') }}">Create Post</a></li>
                                <li>
                                    <a href="{{ route('export') }}" download > Export Posts</a>
                                </li>
                            </ul>
                    </li>
                    <li >
                        <a href="{{route('category')}}"> Category <span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-tags"></span></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>

@yield('content')

<script src="\js\jquery-3.2.1.min.js" type="text/javascript" ></script>
<script src="\bootstrap-3\js\bootstrap.min.js"></script>
<script src="\ckeditor1\ckeditor.js" type="text/javascript" ></script>
<script src="\js\admin\news.js" type="text/javascript" ></script>
<script src="\js\admin\category.js" type="text/javascript" ></script>
<script src="\DataTables\datatables.js" type="text/javascript" ></script>

</body>
</html>