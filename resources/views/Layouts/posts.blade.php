<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>News</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="{{asset('bootstrap-3/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('bootstrap-3/css/bootstrap-theme.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/news.css')}}">
</head>
<body>
<nav class="navbar navbar-default nav-header">
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            <a class="navbar-brand" href="/">NEWS</a>
        </ul>
        @if(Auth::guard('user')->check())
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                        {{ Auth::guard('user')->user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ route('userLogout') }}"
                               onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('userLogout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        @else
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ route('userRegisterView') }}"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><a href="{{ route('userLoginView') }}"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            </ul>
        @endif
    </div>
</nav>

@yield('content')
<script src="\js\jquery-3.2.1.min.js" type="text/javascript" ></script>
<script src="\bootstrap-3\js\bootstrap.min.js"></script>
<script src="\js\site\posts.js" type="text/javascript" ></script>

</body>
</html>