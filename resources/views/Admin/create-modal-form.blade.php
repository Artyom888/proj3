<form method="post" id="createPostForm">
    <div class="form-group">
        <label for="createName" class="control-label">Category Name:</label>
        <input type="text" name="createName" class="form-control" id="createName">
    </div>
    <div class="form-group">
        <label for="createDesc" class="control-label">Category Description:</label>
        <input type="text" name="createDesc" class="form-control" id="createDesc">
    </div>
    <button type="button" class="btn btn-success addCatBtn">Add category</button>

</form>