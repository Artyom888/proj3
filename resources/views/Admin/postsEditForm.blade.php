<form id="editPostForm" method="post">
    <div class="form-group">
        <label for="editPostTitle" class="control-label">New Title Name:</label>
        <input type="text" name="title" value="{{ $result->title }}" class="form-control" id="editPostTitle" >
    </div>
    <div class="form-group">
        <label for="editPostDesc" class="control-label">New Description Name:</label>
        <input type="text" name="desc" value="{{$result->desc}}" class="form-control" id="editPostDesc" >
    </div>
    <div class="form-group">
        <label for="editPostContent" class="control-label">New news content Name:</label>
        <input type="text" name="content" value="{{$result->news_content}}" class="form-control" id="editPostContent" >
    </div>
<div class="form-group">
        <label for="editPostContent" class="control-label">Likes:</label>
        <input type="text" name="content" value="{{$result->likes}}" class="form-control" id="editLikes" >
    </div>

    <button type="button" class="btn btn-success" id="addEditedPost">Accept changes</button>
</form>
