@extends('Layouts.index')
@section('content')

        {{-- Popup for edit admin posts --}}
        <div class="modal fade" id="editPostModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Edit Post</h4>
                    </div>
                    <p id="updatePost"></p>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default closeBtn" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 cont-content ">
        <div class="table-responsive">
            <div class="form-group">
                <label for="cat">Category Filter:</label>
                <select name="cat" id="catFilter">
                    <option>All</option>
                </select>
            </div>
        <table id="newsDtb" class="display" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Created <Time></Time></th>
                <th>Title</th>
                <th>Description</th>
                <th>News Content</th>
                <th>News Likes</th>
                <th>News Image</th>
                <th>Edit</th>
                <th>Options</th>
            </tr>
            </thead>

            <tbody>
            </tbody>
        </table>
        </div>

    </div>

@endsection