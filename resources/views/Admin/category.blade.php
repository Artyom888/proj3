@extends('Layouts.index')
@section('content')

<div class="container cont-content ">
        {{-- Popup for create new category --}}
    <div class="modal fade" id="createCatModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Create new category</h4>
                </div>
                    <p id="createResult"></p>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default catCloseBtn" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    {{-- Popup for Edit category --}}
    <div class="modal fade" id="editCatModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">

        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">Edit category</h4>
                </div>
                <p id="updateCat"></p>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default editCloseBtn" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
{{-- Category Datatable--}}
    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 cont-content ">
        <table id="catDtb" class="display" width="100%" cellspacing="0">
            <thead>
            <tr>
                <th>Created Time</th>
                <th>Category Name</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
        <button type="button" id="catCreateBtn" class="btn btn-default" data-toggle="modal" data-target="#createCatModal">
            Create New Category</button>
        <a href="{{ route('export_categories') }}" download><button type="button" class="btn btn-default">Export</button></a>
    </div>
@endsection