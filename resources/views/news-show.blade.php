@extends('Layouts.posts')
@section('content')
    <div class="container">
        <p> Post created | {{$post->created_at}}</p>
        <h3 class="news-title">{{$post->title}}</h3>
        <p class="news-desc">{{$post->desc}}</p>
        <div>
            @if($post->image)
                <img src="/photos/{{$post->image}}" class="center-block one-post-img" alt="image">
            @else
                <img src="{{asset('photos/unknown_picture.jpg')}}" class="center-block one-post-img" alt="image">
            @endif
        </div>
            @if($post->likes > 1)
                <p class="showLike"> This post has  {{$post->likes}} Likes</p>
            @elseif($post->likes = 1)
                <p class="showLike"> This post has  {{$post->likes}} Like</p>
            @else
                <p class="showLike"> This post doesn't have Like</p>
            @endif
        <p class="short-content">{!! $post->news_content !!}</p>
        <div id="commentsErr"></div>
            @if(Auth::guard('user')->check())
                <div class="addComment">
                    <form method="post">
                        <textarea class="form-control commentText" name="userComment" placeholder="Your comment"></textarea>
                        <button id="addComment" class="btn btn-info" value="{{ $post->id }}">Add</button>
                    </form>
                </div>
            @endif
        @if(!($post->comments)->isEmpty())
            <div class="row">
                <div class="col-sm-12">
                    <h3>User Comments</h3>
                </div>
            </div>
            @foreach($post->comments->reverse() as $comment)
                <div class="row">
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
                        @if($comment->user->image)
                            <div class="thumbnail">
                                <img class="img-responsive user-photo" src="/UserThumbs/5504081541517516291.png">
                            </div>
                        @else
                            <div class="thumbnail">
                                <img class="img-responsive user-photo"
                                     src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
                            </div>
                        @endif
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <strong>Created by`{{$comment->user->name}}</strong>
                                <span> Created Date ` {{ $comment->created_at->format('D-M-Y')}}</span>

                            </div>
                            <div class="panel-body commentContent">
                                {{ $comment->content}}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="row">
                <div class="col-sm-12">
                    <h3>No Comments</h3>
                </div>
            </div>
        @endif
        <a href="/">
            <button type="button" class="btn btn-default btn-mg">
                <span class="glyphicon glyphicon-arrow-left"></span> Back Home Page
            </button>
        </a>
</div>
@endsection