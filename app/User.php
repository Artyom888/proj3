<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'login', 'password','role_id','image',
    ];

    public function role(){
        return   $this->belongsTo('App\Role','role_id');

    }
    public  function likedPosts(){
        return $this->belongsToMany('App\Post','users_likes','user_id','post_id');
    }

    public  function comments(){
        return $this->hasMany('App\Comment','user_id');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
