<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['id','title','desc','news_content','category_id','image','likes'];
    public function ctg(){
        return  $this->belongsTo('App\Category', 'category_id');
    }

    public function likers() {
        return  $this->belongsToMany('App\User','users_likes','post_id','user_id' );
    }

    public function comments(){
        return  $this->hasMany('App\Comment','post_id');
    }


}
