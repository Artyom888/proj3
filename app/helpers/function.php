<?php
function myExport($data,$filename,$fields){

    $file = $filename .".csv";
    $handle = fopen($file, 'w+');
    fputcsv($handle, $fields,';');

    foreach($data as $row) {
        fputcsv($handle, $row,';');
    }
    fclose($handle);

    $headers = array(
        'Content-Type' =>  "application/CSV",
    );

    return response()->download($file, $filename .'.csv', $headers);
}