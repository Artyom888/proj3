<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:100' ,
            'desc' => 'required|max:100' ,
            'news' => 'required',
            'cat' => 'required',
            'image' => 'image|mimes:jpeg,jpg,png,gif'
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'required'=> 'Fill all fields!',
            'image'=> 'Specify correctly image Format!',
            'mimes'=> 'Specify correctly image Format!',
         ];
    }
}
