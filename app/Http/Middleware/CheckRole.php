<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $actions = $request->route()->getAction();
        if(isset($actions['roles'])) {
            $roles = $actions['roles'];
            if (in_array(Auth::guard('admin')->user()->role->name, $roles)) {
                return $next($request);
            } else {
                return redirect()->back();
            }
        } else{
            return $next($request);
        }

    }

}
