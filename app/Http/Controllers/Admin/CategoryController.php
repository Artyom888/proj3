<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /** GET ALL
     * @param Request $request
     * @return string
     */
    public function getAll(Request $request)
    {
        $start = $request->has('start') ? $request->input('start') : 0;
        $limit = $request->has('length') ? $request->input('length') : 10;
        $value = $request['search']['value'];
        $order = $request['order'][0];
        $query = Category::select(['id', 'name', 'description', 'created_at']);
        $totalRecords = $query->count();
        if (!empty($value)) {
            $query->Where('name', 'like', '%' . $value . '%')
                ->orWhere('description', 'like', '%' . $value . '%');
        }
        $recordsFiltered = $query->count();
        switch ($order['column']) {
            case 0 :
                $i = 'created_at';break;
            case 1 :
                $i = 'name';break;
            default:
                $i = 'created_at';
        }
        $ctg = $query->offset($start)->limit($limit)->orderBy($i, $order['dir'])->get()->toArray();
        $res = [
            'data' => $ctg,
            'draw' => $request->input('draw'),
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $recordsFiltered,
        ];
        return json_encode($res);
    }

    /** // Destroy
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $response = ['success' => false];
        $id = $request->input('id');
        if ($id) {
            $ctg = Category::find($id);
            $catPosts = $ctg->posts;
            if ($catPosts) {
                foreach ($catPosts as $post) {
                    if ($post->image != NULL) {
                        $photosPath = public_path() . '/photos/' . $post->image;
                        $thumbsPath = public_path() . '/thumbs/' . $post->image;
                        if (file_exists($photosPath) && file_exists($thumbsPath)) {
                            unlink($photosPath);
                            unlink($thumbsPath);
                        }
                    }
                }
            }
        $result = Category::destroy($id);
            if ($result) {
                $response['success'] = true;
            }
        } else {
            $response['message'] = 'wrong id';
        }
        return response()->json($response);

    }

    /**  View Create Form
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->view('Admin/create-modal-form');
    }

    /** Add
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
    {
        $response = ['success' => false];
        $name = $request->input('name');
        $desc = $request->input('desc');
        try {
            if ($name && $desc) {
                $result = Category::create([
                    'name' => $name,
                    'description' => $desc
                ]);
                if ($result) {
                    $response['success'] = true;
                    $response['message'] = 'The category created';
                }
            } else {
                $response['message'] = 'The category has not created ! Repeat again';
            }
        } catch (\Exception $e) {
            $response['message'] = 'You made a mistake! Try again';
        }
        return response()->json($response);
    }

    /** Edit form view
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function viewEdit(Request $request)
    {
        $id = $request->input('id');
        if ($id) {
            $result = Category::select('name', 'description')->where('id', $id)->first();
            return response(view('Admin/catEditForm', array('result' => $result)), 200);
        } else {
            return response('An error has occurred');
        }
    }

    /** Update
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $response = ['success' => false];
        $name = $request->input('name');
        $desc = $request->input('desc');
        $id = $request->input('id');
        try {
            if ($name && $desc && $id) {
                $result = Category::where('id', $id)->update([
                    'name' => $name,
                    'description' => $desc
                ]);
                if ($result) {
                    $response = ['success' => true];
                    $response['message'] = 'The category updated';
                }
            }
        } catch (\Exception $e) {
            $response['message'] = 'This category name already exists!';
        }
        return response()->json($response);
    }

    /** Export
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export()
    {
        $data = Category::select('name', 'description')->get()->toArray();
        $result = myExport($data, 'categories', array(
                                  'name', 'description',
        ));
        return $result;
    }
}
