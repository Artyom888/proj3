<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Post;
use App\UserLike;
use App\Comment;
use Carbon\Carbon;
use DB;
use Intervention\Image\Facades\Image;
use Auth;
use App\Http\Requests\AddPostRequest;
class PostsController extends Controller
{
    /** Get All
     * @param Request $request
     * @return string
     */
    public function getAll(Request $request){
        $start = $request->has('start') ? $request->input('start') : 0;
        $limit = $request->has('length') ? $request->input('length') : 10;
        $value = $request['search']['value'];
        $order = $request['order'][0];
        $ctgId = $request->input('ctg');
        $query = Post::with('ctg');
        $totalRecords = $query->count();
        if (!empty($value)) {
            $query->Where('title', 'like', '%' . $value . '%')
                ->orWhere('desc', 'like', '%' . $value . '%')
                ->orWhere('news_content', 'like', '%' . $value . '%')
                ->orWhere('likes', 'like', '%' . $value . '%')
            ;
        }
        if($ctgId && $ctgId !== 'All' && empty($value)) {
            $query->where('category_id',$ctgId);// filter
        }
        $recordsFiltered = $query->count();
        switch ($order['column']){
            case 0: $i = 'created_at' ;break;
            case 1: $i = 'title' ;break;
            case 2: $i = 'desc' ;break;
            case 3: $i = 'news_content' ;break;
            case 4:$i = 'likes';  break;
            default: $i = 'created_at';
        }
        $posts = $query->offset($start)->limit($limit)->orderBy($i,$order['dir'])->get()->toArray();
        $res = [
            'data' => $posts,
            'draw' => $request->input('draw'),
            'recordsTotal' => $totalRecords,
            'recordsFiltered' => $recordsFiltered,
        ];
        return json_encode($res);
    }

    /**  Add post
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addPost(Request $request){
        $messages = [
            'required'=> 'Fill all fields!',
        ];
        $rules = [
            'title' => 'required|max:100' ,
            'desc' => 'required|max:100' ,
            'news' => 'required',
            'cat' => 'required'
        ];
        if($request->hasFile('image')){
            $rules['image'] = 'image|mimes:jpeg,jpg,png,gif';

        }
        $this->validate($request,$rules,$messages);
        $title = $request->input('title');
        $desc = $request->input('desc');
        $news = $request->input('news');
        $category = $request->input('cat');
        $params = [
            'title' => $title,
            'desc' => $desc,
            'news_content' => $news,
            'category_id' => $category
        ];
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $currentTimestamp = Carbon::now()->timestamp;
            $imgName = rand().$currentTimestamp;
            $extension = $file->getClientOriginalExtension();
            $thumbImg = Image::make($file->getRealPath())->resize(70, 50);
            $thumbImg->save('thumbs'.'/'. $imgName.'.'.$extension,100);
            $file->move('photos', $imgName.'.'.$extension);
            $params['image'] = $imgName.'.'.$extension ;
        }
        $create = Post::create($params);
        if($create){
            return redirect()->back();
        }
    }

    /** Admin post edit form view
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function view(Request $request)
    {
        $id = $request->input('id');
        if ($id) {
            $result = Post::select('title', 'desc','news_content','likes','category_id')->where('id', $id)->first();
            return response(view('Admin/postsEditForm',array('result'=> $result)),200);
        } else {
            return response('An error has occurred');
        }
    }

    /**  Add post edit
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePost(Request $request){
        $response = ['success' => false];
        $id = $request->input('id');
        $title = $request->input('title');
        $desc = $request->input('desc');
        $content = $request->input('content');
        $likes = $request->input('likes');
        if($title && $desc && $content && $likes && $id){
            $result = Post::where('id',$id)->update([
                'title' => $title,
                'desc' => $desc,
                'news_content' => $content,
                'likes' => $likes
            ]);
            if($result){
                $response['success'] = true;
                $response['message'] = 'The post updated';
            }
        }else{
            $response['message'] = 'The post has not updated ! Repeat again';
        }
        return response()->json($response);

    }
    /** news Dtb select option
     * @return string
     */
    public function select(){
        $catFilter = Category::select('id','name','description')->get()->toArray();
        return json_encode($catFilter);
    }

    /** Delete post
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $response = ['success' => false];
        $id = $request->input('id');
        if ($id) {
            $imgName = Post::find($id);
            if($imgName->image != NULL){
                $photosPath = public_path().'/photos/'.$imgName->image;
                $thumbsPath = public_path().'/thumbs/'.$imgName->image;
                if(file_exists($photosPath) && file_exists($thumbsPath)) {
                    unlink($photosPath);
                    unlink($thumbsPath);
                }
            }
            $result = Post::destroy($id);
            if($result) {
                $response['success'] = true ;
            }
        }else{
            $response['message'] = 'wrong id';
        }
        return response()->json($response);
    }

    /** Get Category for create post form

     * @return mixed
     */
    public function getCatForCreate()
    {
        $catResult = Category::select('id','name')->get();
        return view('Admin\create-post')->withCatRes($catResult);
    }
    /** Export posts table
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(){
        $data = Post::select('title','desc','news_content','image','likes','category_id')->get()->toArray();
        $result = myExport($data,'posts',array(
            'title', 'desc','news content','image','likes', 'category id'
        ));

        return $result;

    }
}
