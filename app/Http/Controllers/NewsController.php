<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;
use App\Comment;
use Config;
class NewsController extends Controller
{
    /** Show All posts
     * @return $this
     */

    public function getAll(){
        $query = Post::select('id','title','desc','image','likes')->orderBy('created_at','desc');
        $posts = $query->paginate(10);
    return view('news')->with('posts',$posts);
    }

        /** Show one post
         * @param $id
         * @return $this
         */

    public function show($id){
            $result = Post::select('id','title','desc','news_content','image','likes','created_at')->where('id',$id)->first();
        return view('news-show')->with(['post'=> $result ]);
        }

        /** Posts likes
         * @param Request $request
         * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
         */

    public function updateLikes(Request $request){
            $response = ['success' => false];
            $postId = $request->input('id');
            if($postId) {
                $query = Post::where('id', $postId)->increment('likes');
                $quantity = Post::select('likes')->where('id', $postId)->first()->toArray();
                Auth::guard('user')->user()->likedPosts()->attach($postId);
                if($query){
                    $response = ['success' => true];
                    $response['message'] = 'The post like updated';
                    $response['quantity'] = $quantity;
                }
            }else{
                $response['message'] = 'The post like not updated:error';
            }
            return response($response);
        }

        /** Create User comment
         * @param Request $request
         * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
         */

    public function createComment(Request $request){
            $response = ['success' => false];
            $postId = $request->input('postId');
            $text = $request->input('text');
            $userId = Auth::guard('user')->user()->id;
            if($text){
                $query =  Comment::create([
                    'content' => $text ,
                    'post_id' => $postId ,
                    'user_id' =>  $userId,
                ]);
                if($query){
                    $response = ['success' => true];
                    $response['message'] = 'Comment successfully added';
                }
            }else{
                $response['message'] = 'The comment has not created ! Repeat again';
            }
            return response($response);
        }

}
