<?php

namespace App\Http\Controllers\UserAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
class LoginController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function login(Request $request){
      $rules =  [
          'login' => 'required|string',
          'password' => 'required|string',
      ];
      $this->validate($request,$rules);
        $login = $request->input('login');
        $password = $request->input('password');
      if (Auth::guard('user')->attempt(['login' => $login, 'password' => $password])) {
          return redirect('/');
      }else{
          return redirect()->back()->withInput()->with('error', 'You entered the wrong login or password!');
      }
  }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout(){
        Auth::guard('user')->logout();
        return redirect()->back();
    }
}
