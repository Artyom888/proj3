<?php

namespace App\Http\Controllers\UserAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
class RegisterController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function register(Request $request)
    {
        $messages = [
            'required'=> 'Fill all fields!',
        ];
        $rules = [
            'name' => 'required|string|max:60',
            'login' => 'required|string|max:60|unique:users',
            'password' => 'required|string|min:6|max:60|confirmed',
        ];
        if($request->hasFile('image')){
            $rules['image'] = 'image|mimes:jpeg,jpg,png,gif';

        }
        $this->validate($request,$rules,$messages);
        $params = [
            'name' => $request->input('name'),
            'login' => $request->input('login'),
            'password' => bcrypt($request->input('password')),
        ];
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $currentTimestamp = Carbon::now()->timestamp;
            $imgName = rand().$currentTimestamp;
            $extension = $file->getClientOriginalExtension();
            $userThumbImg = Image::make($file->getRealPath())->resize(75, 70);
            $userThumbImg->save('UserThumbs'.'/'. $imgName.'.'.$extension,100);
            $params['image'] = $imgName.'.'.$extension ;
        }
        $create = User::create($params);
        if($create)
            return redirect()->route('userLoginView');
    }
}
